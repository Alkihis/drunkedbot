<?php

class ArgParser {
    protected $arguments = []; // saved values for internals arguments
    protected $user_args = []; // $argv
    protected $accepted_args = []; // Combinaison key (user entred arg) => value (internal name)
    protected $arguments_modes = []; // Set mode for argument (by user entered arg name) key => value (mode)

    const ORPHAN = 0; // Orphan argument (written no matter how [post, -post, --post])
    const VALUED = 1; // Valued argument (written no matter how [post, -post, --post])
    const WO_DASH = 2; // O : 2 / V : 3
    const WITH_DASH = 4; // O : 4 / V : 5 | O-wo : 6 / V-wo : 7
    const WITH_DOUBLE_DASH = 8; // O : 8 / V : 9 | O-wo : 10 / V-wo : 11 | O-wi : 12 / V-wi : 13 | O-wo-wi : 14 / V-wi-wo : 15
    // Use ORPHAN or VALUED combined with mode WO_DASH/WITH_DASH/WITH_DOUBLE_DASH
    // to define the mode you want. Example : ArgParser::VALUED + ArgParser::WO_DASH
    // By default, ORPHAN is used
    // Valued arguments : mode % 2 !== 0
    protected const COMBINED = 14; // Values if user have fun to add all constants each others
    protected const COMBINED_VALUED = 15;

    public function __construct(?array $arg = null) {
        $this->initUserArguments($arg);
    }

    protected function initUserArguments(?array $arg = null) : void {
        if(!$arg) {
            global $argv;
            if(isset($argv) && is_array($argv)) {
                $this->user_args = $argv;
            }
            else {
                throw new InvalidArgumentException('$arg is not specified or null, and the global variable $argv is not defined');
            }
        }
        else {
            $this->user_args = $arg;
        }
    }

    protected function validMode(int $mode) : bool {
        return ($mode >= 0 && $mode <= self::COMBINED_VALUED);
    }

    protected function isModeWithNoDash(int $mode) : bool {
        return ($mode === self::WO_DASH || $mode === self::WO_DASH + self::WITH_DASH || $mode === self::WO_DASH + self::WITH_DOUBLE_DASH);
    }

    protected function isModeWithOneDash(int $mode) : bool {
        return ($mode === self::WITH_DASH || $mode === self::WO_DASH + self::WITH_DASH || $mode === self::WITH_DASH + self::WITH_DOUBLE_DASH);
    }

    protected function isModeWithTwoDash(int $mode) : bool {
        return ($mode === self::WITH_DOUBLE_DASH || $mode === self::WITH_DOUBLE_DASH + self::WITH_DASH || $mode === self::WO_DASH + self::WITH_DOUBLE_DASH);
    }

    // Returns true if user_arg accept the current entred mode (w/o dash, with one dash, with two dashes)
    protected function checkMode(string $user_arg, bool $dash1, bool $dash2) : bool {
        $mode = $this->arguments_modes[$user_arg];

        if($mode % 2 !== 0) { // Normalise le mode (savoir si il est en valeur ou pas n'est pas important)
            $mode--;
        }

        if($mode === self::ORPHAN || $mode === self::COMBINED) {
            return true;
        }

        if(!$dash1 && !$dash2) { // On a aucun dash
            return $this->isModeWithNoDash($mode);
        }

        if($dash1 && !$dash2) { // On a que le premier dash
            return $this->isModeWithOneDash($mode);
        }

        if($dash1 && $dash2) { // On a les deux dashs
            return $this->isModeWithTwoDash($mode);
        }

        // Invalid mode
        return false;
    }

    public function getArgument(string $name) { // Mixed return type (type of arg)
        if(array_key_exists($name, $this->arguments)) {
            return $this->arguments[$name];
        }

        return false;
    }

    // Change the argument mode (with value / without value [orphan])
    public function setArgumentMode(string $arg_user_name, int $mode) : void {
        if(!$this->validMode($mode)) {
            throw new InvalidArgumentException('Specified mode is invalid for argument ' . $arg_user_name);
        }

        if(array_key_exists($arg_user_name, $this->arguments_modes)) {
            $this->arguments_modes[$arg_user_name] = $mode;
        }
        else {
            throw new RangeException("Argument $arg_user_name doesn't exist");
        }
    }

    // Set argument mode for all user args matching the given internal name
    public function setGlobalArgumentMode(string $arg_internal_name, int $mode) : void {
        if(!$this->validMode($mode)) {
            throw new InvalidArgumentException('Specified mode is invalid for argument ' . $arg_user_name);
        }

        foreach($this->accepted_args as $arg_user_name => $internal_name) {
            if($internal_name === $arg_internal_name) {
                $this->arguments_modes[$arg_user_name] = $mode;
            }
        }
    }

    public function buildArguments() : void {
        $arg_count = count($this->user_args);
        $wait_value = false;
        $in_waiting_arg = '';

        for($i = 1; $i < $arg_count; $i++) {
            if($wait_value) {
                // Si on attend une valeur à placer dans un argument
                $wait_value = false;
                $this->arguments[$in_waiting_arg] = $this->user_args[$i];
                $in_waiting_arg = '';
            }
            else {
                // Recherche un argument
                $matches = [];
                if(preg_match('/(-?)(-?)(.+)/', $this->user_args[$i], $matches)) {
                    // On recherche si l'argument existe
                    $one_dash = (!empty($matches[1]));
                    $two_dash = (!empty($matches[2]));
                    $user_entered_arg = $matches[3];

                    if(array_key_exists($user_entered_arg, $this->accepted_args) && $this->checkMode($user_entered_arg, $one_dash, $two_dash)) {
                        // On vérifie que le mode de l'argument est bon et que l'argument est dans la liste des acceptés
                        if(!array_key_exists($this->accepted_args[$user_entered_arg], $this->arguments)) {
                            // On cherche à l'enregistrer si il n'a pas déjà été défini
                            $internal_name = $this->accepted_args[$user_entered_arg];
                            
                            if($this->arguments_modes[$user_entered_arg] % 2 !== 0) { // Tous les modes avec valeurs sont des chiffres impairs
                                // On recherche si l'argument attend une valeur
                                $wait_value = true;
                                $in_waiting_arg = $internal_name;
                            }
                            else {
                                $this->arguments[$internal_name] = true;
                            }
                        } 
                    }
                }
            }
        }
    }

    public function showActiveArguments() : void {
        echo "Arguments prêts à être utilisés [nom utilisateur : nom interne]\n";
        foreach($this->accepted_args as $user_name => $internal_name) {
            echo "$user_name => $internal_name (mode : {$this->arguments_modes[$user_name]})" . ($this->arguments_modes[$user_name] % 2 === 0 ? ' [argument orphelin]' : '') . "\n";
        }

        echo "Arguments initialisés par l'utilisateur [nom interne : valeur]\n";
        foreach($this->arguments as $internal_name => $value) {
            echo "$internal_name => " . (is_string($value) ? $value : (int)$value) . "\n";
        }
    }

    public function addArgument($arg_possible_name, string $internal_name, int $mode = self::ORPHAN) : void { // arg_possible_name is array of string or string only
        if(is_string($arg_possible_name)) {
            $arg_possible_name = [$arg_possible_name];
        }

        if(!is_array($arg_possible_name)) {
            throw new InvalidArgumentException("Argument name(s) are invalidely formed : String or array of string accepted only");
        }
        else if(!$this->validMode($mode)) {
            throw new InvalidArgumentException("Invalid mode specified");
        }
        foreach($arg_possible_name as $a) {
            $arg_string = $a;
            $arg_mode = $mode;

            if(is_array($a)) { // Si on a passé un argument tel que ['nom', mode] au lieu de 'nom'
                $arg_string = $a[0];

                if(count($a) > 1) {
                    if($this->validMode((int)$a[1])) {
                        $arg_mode = (int)$a[1];
                    }
                    else {
                        throw new InvalidArgumentException("Invalid mode specified for argument $arg_string");
                    }
                }
            }

            if(is_string($arg_string)) {
                if(array_key_exists($arg_string, $this->accepted_args)) {
                    throw new ErrorException("'" . $arg_string . "' is already present into argument list");
                }
                $this->accepted_args[$arg_string] = $internal_name;
                $this->arguments_modes[$arg_string] = $arg_mode;
            }
            else {
                throw new InvalidArgumentException("'$arg_string' is not a valid string");
            }
        }
    }

    public function deleteArgument(string $arg_user_name) : void {
        if(isset($this->arguments_modes[$arg_user_name])) {
            unset($this->arguments_modes[$arg_user_name]);
        }

        if(isset($this->accepted_args[$arg_user_name])) {
            unset($this->accepted_args[$arg_user_name]);
        }
    }

    public function resetArguments() : void {
        $this->arguments = [];
        $this->accepted_args = [];
        $this->arguments_modes = [];
    }

    public function resetUserArguments(?array $arg = null) : void {
        $this->user_args = [];

        $this->initUserArguments($arg);
    }
}
