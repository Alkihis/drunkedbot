<?php

// Objet représentant un liste de tweets organisés du plus récent au plus vieux
// ----------------------------------------------------------------------------

class Timeline implements Countable, Iterator, ArrayAccess {
    protected $tweets = [];

    public function __construct(?array $array_tweets = null, ?string $import_timeline_from_file = null) {
        if($array_tweets){
            foreach($array_tweets as $key => $t) {
                if(!$this->addTweet($t)) {
                    $GLOBALS['log']->write("Le tweet $key n'est pas détecté comme tweet.");
                }
            }
        }
        elseif($import_timeline_from_file) {
            try {
                $tm = json_decode(file_get_contents($import_timeline_from_file));

                foreach($tm as $key => $t) {
                    if(!$this->addTweet($t)) {
                        $GLOBALS['log']->write("Le tweet $key n'est pas détecté comme tweet.");
                    }
                }
            } catch (Exception $e) {
                $this->tweets = [];
            }
        }
    }

    public function addTweet(stdClass $tweet) : ?string { // Retourne l'ID du tweet ou null si le tweet est mal formé
        if($this->isATweet($tweet)) {
            if(!isset($this->tweets[$tweet->id_str])) {
                $this->tweets[$tweet->id_str] = $tweet;
            }
    
            return $tweet->id_str;
        }
        return null;
    }

    public function deleteTweet(string $id) : bool {
        if(isset($this->tweets[$id])) {
            unset($this->tweets[$id]);
            return true;
        }
        return false;
    }

    public function pickRandomTweets(int $count = 1) : ?array {
        $co = count($this->tweets);
        if($count < 1 || $co < $count) { // Vérifie que le count est valide et possible
            return null;
        }

        $i = 0; // Nombre de tweets déjà tirés
        $picked_pos = []; // Position des tweets pris
        $final = []; // Tweets à renvoyer
        $keys_tweets = array_keys($this->tweets);

        while($i < $count) {
            $random_pos = mt_rand(0, ($co - 1));

            if(!in_array($random_pos, $picked_pos)) {
                $final[] = $this->tweets[$keys_tweets[$random_pos]];
                $picked_pos[] = $random_pos;
                $i++;
            }   
        }

        return $final;
    }

    public function getTweetById(string $id) {
        if(isset($this->tweets[$id])) {
            return $this->tweets[$id];
        }
        return null;
    }

    public function saveTimeline(string $filename, bool $overwrite_if_exists = false) : bool {
        if(!$overwrite_if_exists && file_exists($filename)) {
            return false;
        }

        try {
            file_put_contents($filename, $this->getJSONTimeline());
        } catch (Exception $e) {
            return false;
        }
        return true;
    }

    public function getJSONTimeline(bool $id_keys = false) : string {
        return json_encode(($id_keys ? $this->tweets : array_values($this->tweets)));
    }

    static public function isATweet(stdClass $tweet) : bool {
        return isset($tweet->id_str, 
                     $tweet->full_text,
                     $tweet->user->screen_name,
                     $tweet->user->id_str,
                     $tweet->created_at,
                     $tweet->favorite_count,
                     $tweet->retweet_count);
    }

    static public function isARetweet(stdClass $tweet) : bool {
        return isset($tweet->retweeted_status) && Timeline::isATweet($tweet->retweeted_status);
    }

    // Iterator interface implementation
    public function rewind() {
        reset($this->tweets);
    }

    public function current() {
        $var = current($this->tweets);
        return $var;
    }

    public function key() {
        $var = key($this->tweets);
        return $var;
    }

    public function next() {
        $var = next($this->tweets);
        return $var;
    }

    public function valid() {
        $key = key($this->tweets);
        $var = ($key !== NULL && $key !== FALSE);
        return $var;
    }

    // Countable interface implementation
    public function count() { 
        return count($this->tweets); 
    }

    // ArrayAccess interface implementation
    public function offsetSet($offset, $value) {
        throw new BadMethodCallException('You cannot set a value by using [].');
    }

    public function offsetExists($offset) {
        return isset($this->tweets[$offset]);
    }

    public function offsetUnset($offset) {
        unset($this->tweets[$offset]);
    }

    public function offsetGet($offset) {
        return isset($this->tweets[$offset]) ? $this->tweets[$offset] : null;
    }
}
