<?php

require('src/Log.php');
require('src/const.php');
require('src/functions.php');
require('src/Timeline.php');
require('src/Twitter.php');
require('src/Tweet.php');
require('src/ArgParser.php');

// ini_set('display_errors', 'On');

//////////
// MAIN //
//////////

$parser = new ArgParser();
$parser->addArgument('v', 'verbose');
$parser->addArgument(['t'], 'test');
$parser->addArgument(['s'], 'save');
$parser->addArgument(['answer'], 'answer');
$parser->addArgument('tweet', 'tweet', ArgParser::VALUED);
$parser->addArgument(['post'], 'posting');
$parser->addArgument(['dt', 'disable-tweeting'], 'disable_tweet');
$parser->addArgument('ds', 'disable-saving', ArgParser::WITH_DASH);
$parser->buildArguments();

if($parser->getArgument('test')) {
    // $parser->showActiveArguments(); // exit();
    $twitter = new Twitter(OAUTH_KEY, OAUTH_SECRET);
    $twitter->setToken(CONSUMER_KEY, CONSUMER_SECRET);
    $GLOBALS['log'] = new Log(false, true);
    $log = &$GLOBALS['log'];

    $id = $twitter->sendChunkedMedia(BOT_USER_DIR . 'dab/michel.mp4');
    var_dump($id);

    if($id) {
        // $twitter->sendTweet('michmichMICHEL (déosolé)', null, null, null, $id);
    }
    exit();
}

$verbose = $parser->getArgument('verbose');
$test = $parser->getArgument('test');
$answer = $parser->getArgument('answer');
$disable_tweeting = $parser->getArgument('disable_tweet');

$GLOBALS['log'] = new Log($answer, $verbose);
$log = &$GLOBALS['log'];
$log->write('Pourcentage d\'alcoolisation : ' . PERCENTAGE_DRUNK);
// Connecte la base de données
connectBD();

try {
    // Création de l'objet capable d'interagir avec Twitter
    $twitter = new Twitter(OAUTH_KEY, OAUTH_SECRET);
    $twitter->setToken(CONSUMER_KEY, CONSUMER_SECRET);

    if($parser->getArgument('posting')) { // Post de tweet (toutes les 30 minutes)
        $log->write('Mode : Post de tweet.');
        $list = null;
        if($test) {
            $list = new Timeline(null, '/home/pi/timeline_json_test.savtim');
        }
        else {
            $list = $twitter->getList(!$disable_tweeting); // Retourne un objet Timeline
            if($parser->getArgument('save'))
                $list->saveTimeline('/home/pi/timeline_json_test.savtim', true);
        }
        
        if($list) {
            $tries = 0;
            $sended_tweet = false;

            do {
                $log->write('Début du traitement.');

                $tweets_count = count($list);
                $to_fetch = tweetToFetchCount($tweets_count); // Détermine le nombre de tweets à fetch
    
                $tweet_array = $list->pickRandomTweets($to_fetch);
    
                if($tweet_array) {
                    // Suppression des mentions des tweets récupérés
                    do {
                        if(empty($tweet_array)) {
                            $tweet_array = $list->pickRandomTweets(1);
                            $to_fetch = 1;
                        }
                        $has_unsetted_sth = false;
    
                        foreach($tweet_array as $key => $tw) {
                            $new_text = deleteMentions($tw->full_text);
                            if($new_text){
                                $tweet_array[$key]->original_full_text = $tweet_array[$key]->full_text;
                                $tweet_array[$key]->full_text = $new_text;
                            }
                            else {
                                unset($tweet_array[$key]);
                                $has_unsetted_sth = true;
                                $to_fetch--;
                            }
                        }

                        if($has_unsetted_sth) {
                            // On réordonne les indices
                            $tweet_array = array_values($tweet_array);
                        }
                    } while(empty($tweet_array));
    
                    // Log des tweets sélectionnés et assemblage des tweets 
                    $log->write('Nombre de tweets sélectionnés : ' . $to_fetch);
                    $tweet_string = $tweet_array[0]->full_text;

                    logTweets($tweet_array);
                    $positions = [];
                    $offset = 0;

                    for($i = 0; $i < ($to_fetch - 1); $i++) {
                        $positions[$i] = [];
                        
                        $tweet_string = buildCombinedTweet($tweet_string, $tweet_array[$i+1]->full_text, $positions[$i], $offset);
                        if (isset($positions[$i][0])) {
                            $offset = $positions[$i][0] + 1;
                        }
                    }
    
                    if(IS_DRUNK) { // Si le bot est bourré, on lance le processus de "bourrification"
                        $log->write('Début "bourrification".');
                        $tweet_string = tweetBourrification($tweet_string);
                    }
    
                    $log->write('Tweet : ' . $tweet_string);
                    
                    if(mb_strlen($tweet_string) <= 280) {
                        if(!$test && !$disable_tweeting) {
                            $sended_tweet = $twitter->sendTweet(html_entity_decode($tweet_string));

                            if(!$parser->getArgument('disable-saving'))
                                saveTweetsDetails($sended_tweet, $tweet_array, ['drunk_percentage' => PERCENTAGE_DRUNK, 'fusion' => $positions]); 
                        }
                        else {
                            $sended_tweet = true;
                        }
                    }
                    else {
                        $log->write('Le tweet dépasse la limite de caractères imposée par Twitter (280 caractères). Le processus de création redémarre.');
                    }
                    $tries++;
                }
                else {
                    $log->write('La timeline semble vide. Fin.');
                    break;
                }
            } while(!$sended_tweet && $tries < 5);

            // Permet de vider le dossier de sources
            autoCleanSources();
        }
        else {
            $log->write('Aucun tweet n\'a été récupéré. Fin.');
        }
    }

    else if($parser->getArgument('answer')) {
        $log->write('Mode : Réponse aux mentions.');

        $mention = $twitter->getMentions(!$test); // Récupère un objet Timeline

        if($mention) { // Si il y a des mentions
            // exit();
            foreach($mention as $m_key => $m) {
                $log->write("Mention tweet ID : $m_key");

                // Vérifie que la mention ne soit pas envoyée par lui-même
                if($m->user->screen_name === $twitter->getCurrentUser()){
                    $log->write("Mention envoyée par le bot lui-même.");
                    continue;
                }

                // Extraction du texte sans les mentions initiales
                $ma = [];
                preg_match('/^((@\w+ ){1,})(.*)/', $m->full_text, $ma);
                $text_from_tweet = $ma[3] ?? $m->full_text;

                $is_not_badly_mentionned = !isset($ma[1]) || (preg_match('/(^@'. BOT_SCREEN_NAME .')/i', $ma[1]) || !preg_match('/@'. BOT_SCREEN_NAME .'/i', $ma[1]));
                // Match si @chuipasbourre est fait en premier ou si le @ du bot n'apparaît pas en premier
                // $log->write("Mentionné correctement : " . (int)$is_not_badly_mentionned . "; mentions : '{$ma[1]}'");

                if (isTweetToB($text_from_tweet)) { // Vérifie si c'est simplement un tweet à bourrifier
                    $text = trim(preg_replace('/^B:(.+)$/i', '$1', $text_from_tweet));

                    $text = tweetBourrification($text);

                    $twitter->replyTo(html_entity_decode($text), $m->id_str);
                }
                else if ($is_not_badly_mentionned && isTweetToSourced($text_from_tweet)) {
                    $text = getSourcesFromTweet($m->in_reply_to_status_id_str, $m->user->screen_name);

                    if(!$text) {
                        $text = "Impossible d'obtenir les sources de ce tweet.";
                    }

                    $twitter->replyTo(html_entity_decode($text), $m->id_str);
                }
                else if ($is_not_badly_mentionned && isTweetToDab($text_from_tweet)) {
                    $media_path = selectRandomDab();

                    $log->write("Réponse dab.");
                    if($media_path) {
                        $twitter->replyTo('', $m->id_str, $media_path);
                    }
                }
                else if ($is_not_badly_mentionned && isTweetToAcab($text_from_tweet)) {
                    $media_path = BOT_USER_DIR . 'dab/01.acab_jpg';

                    $log->write("Réponse acab.");
                    if($media_path) {
                        $twitter->replyTo('', $m->id_str, $media_path);
                    }
                }
                else if ($is_not_badly_mentionned && $m->in_reply_to_screen_name === $twitter->getCurrentUser() && 
                        isTweetToDelete($text_from_tweet, $m->user->screen_name, $m->in_reply_to_status_id_str, $m->user->id_str)) { 
                    // Le Tweet doit être supprimé et l'utilisateur le demandant est autorisé
                    // Vérifie que le tweet à supprimer lui appartient
                    $twitter->deleteTweet($m->in_reply_to_status_id_str);
                }

                else if ($is_not_badly_mentionned) { // On répond à la mention en piochant dans les tweets de l'utilisateur
                    $list = $twitter->getUserTweets($m->user->id_str, $m->user->screen_name);

                    if($list) {
                        $tries = 0;
                        $sended_tweet = false;

                        do {
                            // Ajoute possible un mot au début (chaîne à coller plus tard)
                            $adding = addWordOnBeginning($text_from_tweet);

                            // Détermine le nombre de tweet à récupérer
                            $tweets_count = count($list);
                            $to_fetch = tweetToFetchCount($tweets_count);
                
                            $tweet_array = $list->pickRandomTweets($to_fetch);
                
                            if($tweet_array) {
                                // Suppression des mentions des tweets récupérés, et récupère de nouveaux tweets si jamais ça échoue
                                do {
                                    if(empty($tweet_array)) {
                                        $tweet_array = $list->pickRandomTweets(1);
                                        $to_fetch = 1;
                                    }
                                    $has_unsetted_sth = false;
                
                                    foreach($tweet_array as $key => $tw) {
                                        $new_text = deleteMentions($tw->full_text);
                                        if($new_text){
                                            $tweet_array[$key]->original_full_text = $tweet_array[$key]->full_text;
                                            $tweet_array[$key]->full_text = $new_text;
                                        }
                                        else {
                                            unset($tweet_array[$key]);
                                            $to_fetch--;
                                            $has_unsetted_sth = true;
                                        }
                                    }
                                    
                                    if($has_unsetted_sth) {
                                        // On réordonne les indices
                                        $tweet_array = array_values($tweet_array);
                                    }
                                } while(empty($tweet_array));
                
                                // Assemblage des tweets sélectionnés
                                $log->write('Nombre de tweets sélectionnés : ' . $to_fetch);
                                $tweet_string = $tweet_array[0]->full_text;
                                // Saving picked tweets
                                logTweets($tweet_array);
                                $positions = [];
                                $offset = 0;

                                for($i = 0; $i < ($to_fetch - 1); $i++) {
                                    $positions[$i] = [];

                                    $tweet_string = buildCombinedTweet($tweet_string, $tweet_array[$i+1]->full_text, $positions[$i], $offset);
                                    if (isset($positions[$i][0])) {
                                        $offset = $positions[$i][0] + 1;
                                    }
                                }
                
                                if(IS_DRUNK) { // Si le bot est bourré, on lance le processus de "bourrification"
                                    $log->write('Début "bourrification".');
                                    $tweet_string = tweetBourrification($tweet_string);
                                }
                                // On crée la chaîne finale en lui ajoutant le "mot intéressant" possible trouvé
                                $tweet_string = $adding . ' ' . $tweet_string;

                                $GLOBALS['log']->write("Réponse : $tweet_string");

                                // Envoi du tweet si possible
                                if(mb_strlen($tweet_string) <= 280) {
                                    if(!$test) {
                                        $sended_tweet = $twitter->replyTo(html_entity_decode($tweet_string), $m->id_str); 
                                        if(!$parser->getArgument('disable-saving'))
                                            saveTweetsDetails($sended_tweet, $tweet_array, ['drunk_percentage' => PERCENTAGE_DRUNK, 'fusion' => $positions]);
                                    }
                                    else {
                                        $sended_tweet = true;
                                    }
                                }
                                else {
                                    $log->write('Le tweet dépasse la limite de caractères imposée par Twitter (280 caractères). Le processus de création redémarre.');
                                }
                                $tries++;
                            }
                            else {
                                $log->write('La timeline semble vide. Mention suivante.');
                                break;
                            }

                        } while(!$sended_tweet && $tries < 5); // Répète tant qu'un tweet n'a pas été envoyé et qu'il y a moins de 5 essais
                    }
                    else {
                        $log->write('Aucun tweet n\'a été récupéré. Mention suivante.');
                    }
                }
                else {
                    $log->write("Rien à faire avec cette mention.");
                }
            }

            $log->write('Aucune mention restante.');
        }
    }

    else if($parser->getArgument('tweet')) {
        $twitter->postStatus($parser->getArgument('tweet')); 
    }

} catch (Throwable $e) {
    
}

deconnectBD();
